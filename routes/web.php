<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['uses' => 'HomeController@index'])->name('home');

Route::get('/events', ['uses' => 'FullCalendarController@show'])->name('events');

Route::get('/medias', ['uses' => 'MediasController@show'])->name('medias');


Route::get('/jujitsu', function () {
    return view('jujitsu');
})->name('jujitsu');

Route::get('/club', function () {
    return view('club');
})->name('club');

Route::get('/contact', function () {
    return view('contact.contact');
})->name('contact');

Route::get('/user', function () {
    return view('user');
});

Route::post('/media', "Media@index");

Route::post('/contact/send', ['uses' => 'ContactController@send'])->name('sendContact');

Route::get('/sitemap', function () {
    return view('sitemap');
})->name('sitemap');


// ADMIN

Route::post('api_login',['uses' => 'AdminController@login'])->name('loginPost');

Route::prefix('admin')->get('login', function(){
    if(isset($_COOKIE['api_token'])){
        return redirect()->route('adminDashboard')
            ->with('success', 'Vous êtes déjà connecté');
    }
    return view('admin.login');
})->name('login');

Route::middleware(['checkAuth'])->prefix('admin')->group(function () {

    Route::get('logout', ['uses' => 'AdminController@logout'])->name('logout');

    Route::get('dashboard', ['uses' => 'AdminController@dashboard'])->name('adminDashboard');

    Route::get('media', ['uses' => 'AdminController@medias'])->name('adminMedias');

    Route::get('news', ['uses' => 'AdminController@news'])->name('adminNews');
    Route::get('users', ['uses' => 'AdminController@users'])->name('adminUsers');
    Route::get('messages', ['uses' => 'AdminController@messages'])->name('adminMessages');
    Route::get('newUser', ['uses' => 'AdminController@newUser'])->name('adminNewUser');
    Route::post('addUser', ['uses' => 'AdminController@addUser'])->name('adminAddUser');
    Route::get('newMedia', ['uses' => 'AdminController@newMedia'])->name('adminNewMedia');
    Route::post('addMedia', ['uses' => 'AdminController@addMedia'])->name('adminAddMedia');
    Route::get('newEvent', ['uses' => 'AdminController@newEvent'])->name('adminNewEvent');
    Route::get('addEvent', ['uses' => 'AdminController@addEvent'])->name('adminAddEvent');
    Route::get('newNews', ['uses' => 'AdminController@newNews'])->name('adminNewNews');
    Route::post('addNews', ['uses' => 'AdminController@addNews'])->name('adminAddNews');
});
