<?php

namespace Tests\Feature;

use Tests\TestCase;
class RouteTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {

        $response =$this->get("/medias");
        $response-> assertStatus(200);

        $response =$this->get("/events");
        $response-> assertStatus(200);

        $response =$this->get("/");
        $response-> assertStatus(200);

        $response =$this->get("/sitemap");
        $response-> assertStatus(200);

        $response =$this->get("/contact");
        $response-> assertStatus(200);

        $response =$this->get("/club");
        $response-> assertStatus(200);

        $response =$this->get("/jujitsu");
        $response-> assertStatus(200);

        $response =$this->get("/admin/login");
        $response-> assertStatus(200);

        $response =$this->get("/admin/dashboard");
        $response-> assertStatus(302);

    }
}
