@extends('baseTemplate')

@section('title', 'Le jujistu')

@section('css')
@stop

@section('content')

<div class="container-text">

    <h2>A propos</br></h2>

    <p>Open Jujitsu c'est :</p>

    <ul>
        <li>des Infos Officielles et de dernières minutes</li>
        <li>du partage, de l'échange</li>
    </ul>

    <p>Le jujitsu étant très complet, Open Jujitsu n'en reste pas moins ouvert aux autres disciplines 
    qu'il intègre à son art d'origine pour évoluer.</p>

    <p>Open Jujitsu se propose comme une page regroupant :</p>

    <ul>
        <li>des Infos Officielles et de dernières minutes</li>
        <li>du partage et de l'échange autour du jujitsu</li>
        <li>une culture Jujitsu apolitique</li>
        <li>une ouverture d'esprit aux autres arts martiaux</li>
    </ul>
        
    <p>Bref, si vous aussi vous aimez le Jujitsu, et le pratiquer, alors n'hésitez pas à prendre contact, 
    peu importe votre affiliation, vos autres pratiques...</br>
    Le jujitsu (traditionnel, comme spécialisé JJB) c'est avant tout des gens motivés qui aiment partager 
    leur passion, leur sport, leur art de vivre...</p>

    <h2>Affiliations</h2>

    <p>EAJJ, FEKAMT, FFJDA, SASG, JJST, ICON, ATUAL CAPOEIRA, KJA</p>

</div>

@stop

@section('script')
@stop