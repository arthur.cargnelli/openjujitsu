@extends('baseTemplate')

@section('title', 'Contact')

@section('css')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
crossorigin=""/>
@stop

@section('content')

    <div class="container align-middle">
        <div class="row lg-col-12 md-col-12 sm-col-12 justify-content-center">
            {{-- Card Information --}}
            <div class="card lg-col-12 md-col-12 sm-col-12 contact-card">
                <div class="card-body">
                    <div class="row justify-content-center">
                        <div class="lg-col-12 md-col-12 sm-col-12">
                            <h5 class="card-title contact fs-30">Nous rencontrer</h5>
                        </div>
                    </div>
                    <div class="row justify-content-around">
                        <div class="lg-col-5 md-col-5 sm-col-5">
                            <h6 class="jujitsu-underline text-center font-weight-bold fs-20">Open <span class="jujitsu">Jujitsu</span></h6>
                            <p>
                                51 Avenue de Madran, 33600 Pessac <br>
                                05 56 36 94 49 <br>
                                <a href="mailto:contact@openjujitsu.fr">contact<span class="jujitsu">@</span>openjujitsu.fr</a>
                            </p>
                        </div>
                        <div class="lg-col-2 md-col-2 sm-col-2">

                        </div>
                        <div class="lg-col-5 md-col-5 sm-col-5">
                            <h6 class="jujitsu-underline text-center fs-20">Horaires accueil et inscription :</h6>
                            <p>
                                <span class="fw-600">Lundi,Mardi,Jeudi,Vendredi:</span> 17h à 20h <br>
                                <span class="fw-600">Mercredi:</span> 14h à 20h30 <br>
                                <span class="fw-600">Samedi:</span> 14h à 19h
                            </p>
                        </div>
                    </div>
                    <div class="row fill-availlable">
                        <div id="map" class="lg-col-12 sm-col-12 text-center map fill-availlable"></div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Social Media --}}

        <div class="row lg-col-12 md-col-12 sm-col-12 justify-content-center">
            {{-- Card Information --}}
            <div class="card lg-col-12 md-col-12 sm-col-12 contact-card">
                <div class="card-body">
                    <div class="row justify-content-around">
                        <div class="lg-col-3 md-col-3 sm-col-3">
                            {{-- Logo Facebook --}}
                            <a href="https://www.facebook.com/openjujitsu/" target="blank"><img class="logo logo-contact" src='/img/facebook.webp' alt="facebook-logo"/></a>
                        </div>
                        <div class="lg-col-6 md-col-6 sm-col-6">
                            <h5 class="card-title contact fs-30">Nous suivre</h5>
                        </div>
                        <div class="lg-col-3 md-col-3 sm-col-3">
                            {{-- Logo Instagram --}}
                            <a href="https://www.instagram.com/openjujitsu/" target="blank"><img class="logo logo-contact" src='/img/instagram.webp' alt="instagram-logo"/></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Form Contact --}}

        <div class="row lg-col-12 md-col-12 sm-col-12 justify-content-center">
            {{-- Card Information --}}
            <div class="card lg-col-12 md-col-12 sm-col-12 contact-card">
                <div class="card-body">
                    <div class="row justify-content-center">
                        <h5 class="text_center mt-5p fs-30"> Nous contacter </h5>
                    </div>
                    <div class="row contact-form lg-col-12 md-col-12 sm-col-12 justify-content-around">
                    <form action="{{route('sendContact')}}" method="post" class="lg-col-12 md-col-12 sm-col-12 fill-availlable mlr-30">
                            @csrf
                            <input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response" class="lg-col-12 md-col-12 sm-col-12" /><br >
                                <div class="row form-group lg-col-12 md-col-12 sm-col-12 fill-availlable">
                                    <div class="lg-col-12 md-col-12 sm-col-12 fill-availlable">
                                        <label for="lastname">Nom</label>
                                        <input type="text" class="form-control" name="firstname" placeholder="Prénom"/>
                                        <input type="text" class="form-control" name="lastname" placeholder="Nom"/>
                                    </div>
                                </div>
                                <div class="row form-group fill-availlable">
                                    <div class="lg-col-12 md-col-12 sm-col-12 fill-availlable">
                                        <label for="email">Email<span class="required">*</span></label>
                                        <input type="email" class="form-control" name="email" placeholder="Email"/>
                                    </div>
                                </div>
                                <div class="row form-group fill-availlable">
                                    <div class="lg-col-12 md-col-12 sm-col-12 fill-availlable">
                                        <label for="body">Message<span class="required">*</span></label>
                                        <textarea name="body" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="row form-group fill-availlable">
                                    <div class="lg-col-12 md-col-12 sm-col-12 fill-availlable">
                                        <label for="phone">Téléphone</label>
                                        <input type="text" name="phone" class="form-control" placeholder="xx.xx.xx.xx.xx">                                    </div>
                                </div>
                                {{-- TODO Implement recaptcha --}}
                                <div class="row">
                                    <div class="lg-col-4 md-col-4 sm-col-4 d-flex justify-content-center align-content-center text-center"></div>
                                    <button type="submit" class="btn btn-primary">Envoyer le message</button>
                                </div>
                         </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
<script src="https://www.google.com/recaptcha/api.js?render=6LducugUAAAAACMYP2NbAlzGkIrZBzHqzFbUFlit"></script>
<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
crossorigin=""></script>
<script type="text/javascript">
    var mymap = L.map('map').setView([44.8056977,-0.6610033], 17);
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1Ijoib3Blbmp1aml0c3UiLCJhIjoiY2s4dWI5bWJoMDJ0ZzNnbDE3YmM1ZTlycCJ9.MfIG6MOztqnnxErit7reCA', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'your.mapbox.access.token'
    }).addTo(mymap);
    var marker = L.marker([44.8056977,-0.6610033]).addTo(mymap);
    marker.bindPopup("<b>OpenJujitsu</b><br>51 Avenue de Madran, 33600 Pessac ").openPopup();
</script>
<script>
grecaptcha.ready(function() {
    grecaptcha.execute('6LducugUAAAAACMYP2NbAlzGkIrZBzHqzFbUFlit', {action: 'homepage'})
    .then(function(token) {
    document.getElementById('g-recaptcha-response').value=token;
    });
});
</script>
@stop
