@extends('baseTemplate')

@section('title', 'Medias')

@section('css')
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @foreach($medias as $media)
        <figure class="col-sm-4">
            <a href="{{ $media["path"] }}">
            <img src="{{ $media["path"] }}" alt="{{ $media["description"] }}" class="img-fluid">
            </a>
        </figure>
        @endforeach
    </div>
</div>
@stop

@section('script')
@stop
