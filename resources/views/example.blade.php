@extends('exampleTemplate')
    @section('meta')
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    @endsection

    @section('css')
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    @endsection

    @section('title', "example")
    @section('content')
        <h1>Hello, world!</h1>
    @endsection
    @section('script')
        <script src="{{ mix('js/home.js') }}"></script>
    @endsection
