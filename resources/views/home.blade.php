@extends('baseTemplate')

@section('title', 'Accueil')

@section('css')

@stop

@section('content')

<script async defer crossorigin="anonymous" src="https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v6.0&appId=1146882228980106&autoLogAppEvents=1"></script>

<div class="container">
    {{-- Info --}}
    <div class="row">
        {{-- Implementation Information TODO --}}
        <fieldset class="p-2 last_infos lg-col-12 md-col-12 sm-col-12">
            <legend class="w-auto">
                <h2>&nbsp Infos : &nbsp</h2>
            </legend>
            <ul>
                @foreach($lastNews as $news)
                    <li>{{ $news['title'] }} : {{ $news['description'] }}</li>
                @endforeach
            </ul>
        </fieldset>
    </div>

    {{-- News --}}
    <div class="row">
        <div class="row lg-col-12 sm-col-12 fill-availlable">
            <fieldset class="separator-news lg-col-12 sm-col-12">
                <legend class="w-auto">
                    <h2>&nbsp News &nbsp </h2>
                </legend>
            </fieldset>
        </div>
        <div class="row">
            {{-- FACEBOOK --}}
            <div class="col-lg-6">
                <div class="fb-page" data-href="https://www.facebook.com/openjujitsu/" data-tabs="timeline" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                    <blockquote cite="https://www.facebook.com/openjujitsu/" class="fb-xfbml-parse-ignore">
                        <a href="https://www.facebook.com/openjujitsu/">Open Jujitsu</a>
                    </blockquote>
                </div>
            </div>

            {{-- LAST MEDIA --}}
            <div class="col-lg-6">
                <h2 class="text-center">Les derniers médias</h2>
                <div class="card card-news">
                    <div class="d-flex flex-row">
                        <div class="flex-column">
                            <a href="{{ $lastMedias[0]['path'] }}">
                                <img src="{{ $lastMedias[0]['path'] }}" alt="{{ $lastMedias[0]['description'] }}" class="img-fluid">
                            </a>
                            <a href="{{ $lastMedias[2]['path'] }}">
                                <img src="{{ $lastMedias[2]['path'] }}" alt="{{ $lastMedias[2]['description'] }}" class="img-fluid">
                            </a>
                        </div>
                        <div class="flex-column">
                            <a href="{{ $lastMedias[1]['path'] }}">
                                <img src="{{ $lastMedias[1]['path'] }}" alt="{{ $lastMedias[1]['description'] }}" class="img-fluid">
                            </a>
                            <a href="{{ $lastMedias[3]['path'] }}">
                                <img src="{{ $lastMedias[3]['path'] }}" alt="{{ $lastMedias[3]['description'] }}" class="img-fluid">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop


@section('script')
@stop
