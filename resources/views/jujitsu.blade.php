@extends('baseTemplate')

@section('title', 'Le jujistu')

@section('css')
@stop

@section('content')
<div class="container-text">
    <div class="row">
        <h1>Le jujistu</h1>
        
        <p class="p-jujitsu">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu placerat tellus. Donec sit 
            amet dapibus magna. Nunc faucibus sapien ipsum, vel cursus libero varius in. Pellentesque 
            eleifend purus nec nisi sagittis semper. Curabitur ac diam vitae nisl cursus finibus quis in 
            arcu. Mauris sed dignissim risus. Vivamus accumsan, metus eu dictum hendrerit, quam quam aliquam 
            turpis, eget rhoncus quam libero sit amet sapien.
        </p>

        <h2>Art de la souplesse</h2>

        <p class="p-jujitsu">Nunc tellus est, pretium eu maximus vitae, volutpat at magna. Pellentesque dignissim tortor eget 
            blandit luctus. Nullam magna dolor, laoreet et posuere vitae, laoreet in eros. Nunc consectetur 
            imperdiet velit ultrices congue. Nunc nulla lorem, eleifend ut feugiat at, ultricies non lacus. 
            Sed consequat purus nec metus vehicula tincidunt vehicula vel turpis. Suspendisse tristique 
            iaculis magna, at dictum metus tempus vel. Quisque non massa molestie, dapibus ante eget, cursus 
            tortor. Fusce ut erat lorem.
        </p>

        <h2>Méthode de défense</h2>

        <p class="p-jujitsu">Fusce interdum, erat id malesuada bibendum, massa diam volutpat ligula, nec mollis magna lectus 
            ut justo. Pellentesque faucibus purus et tortor pellentesque auctor. Maecenas tellus mi, lacinia 
            at suscipit vel, pharetra vel nisl. Aliquam sit amet nulla tempor, mollis dolor quis, egestas 
            tortor. Nullam ac odio in ex tristique efficitur vitae et dui. Aliquam erat volutpat. Aliquam 
            suscipit massa in nulla sollicitudin molestie.
        </p>
    </div>
</div>
@stop

@section('script')
@stop