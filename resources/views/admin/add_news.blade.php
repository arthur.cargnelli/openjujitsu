@extends('admin.baseAdminTemplate')

  @section('title', 'Add news')

  @section('css')
  @stop

  @section('content')

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User inscription
        <small>Data Dashboard</small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Ajouter une News</h3>
          <br>
<form action={{ route('adminAddNews') }} method="POST">

    @csrf
   <div class="form-group">
    <label for="title">Title</label>
    <input type="text" name='title' class="form-control" id="exampleFormControlInput1" placeholder="Name" required>
  </div>

 <div class="form-group">
    <label for="description">Description</label>
    <input type="text" name="description" class="form-control" id="exampleFormControlInput1" placeholder="" required>
  </div>

   <div class="form-group">
    <label for="linkURL">Url</label>
    <input type="text" name="linkURL" class="form-control" id="exampleFormControlInput1" placeholder="Login">
  </div>

 <button type="submit" class="btn btn-primary mb-2">Valider</button>
</form>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @stop

  @section('script')
  @stop

