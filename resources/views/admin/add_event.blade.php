@extends('admin.baseAdminTemplate')

  @section('title', 'Add Event')

  @section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
  @stop

  @section('content')<!DOCTYPE html>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User inscription
        <small>Data Dashboard</small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Ajouté un Evénement</h3>
          <br>
<form action={{ route('adminAddEvent') }}>
    @csrf

   <div class="form-group">
    <label for="exampleFormControlInput1">Name</label>
    <input type="text" name='event_name' class="form-control" id="exampleFormControlInput1" placeholder="Name" required>
  </div>

 <div class="form-group">
    <label for="exampleFormControlInput1">Description</label>
    <input type="text" name='description' class="form-control" id="exampleFormControlInput1" placeholder="" required>
  </div>

   <div class="form-group">
    <label for="exampleFormControlInput1">Date</label>
    <input type="text" name='daterange' class="form-control" id="reservation" placeholder="" required>
  </div>
 <button type="submit" class="btn btn-primary mb-2">Valider</button>
</form>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  @stop

  @section('script')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script>
        $(function() {

        var start = moment().startOf('hour').format("DD/MM/YYYY hh:mm");
        var end =  moment().startOf('hour').format("DD/MM/YYYY hh:mm");

        function cb(start, end) {
            $('#reportrange span').html(start + ' - ' + end);
        }
        var local =  {
            "format": "DD-MM-YYYY hh:mm",
            "separator": " / ",
            "applyLabel": "Appliquer",
            "cancelLabel": "Annulé",
            "fromLabel": "De",
            "toLabel": "à",
            "customRangeLabel": "Customisable",
            "weekLabel": "S",
            "daysOfWeek": [
                "Dim",
                "Lun",
                "Mar",
                "Mer",
                "Jeu",
                "Ven",
                "Sam"
            ],
            "monthNames": [
                "Jan",
                "Fev",
                "Mars",
                "Avr",
                "Mai",
                "Juin",
                "Jui",
                "Août",
                "Sept",
                "Oct",
                "Nov",
                "Dec"
            ],
            "firstDay": 1
        }

        $('input[name="daterange"]').daterangepicker({
            'timePicker': true,
            "locale": local,
            "startDate": start,
            "endDate": end,
            "minDate": moment().startOf('hour').format("DD/MM/YYYY hh:mm"),
            "ranges": {
            }
            }, cb);

            cb(start, end);

            });
    </script>
  @stop

