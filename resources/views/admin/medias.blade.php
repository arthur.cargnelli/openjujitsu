@extends('admin.baseAdminTemplate')

  @section('title', 'Medias')

  @section('css')
  @stop

  @section('content')

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashbord media
        <small>Data Dashboard</small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Title</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
      <table class="table table-striped">
  <thead class="thead-dark">
    <tr>
      <th scope="col">id</th>
      <th scope="col">Date de ceation</th>
      <th scope="col">Description</th>
      <th scope="col">URL</th>
      <th scope="col">Type de media</th>
       <th scope="col">Option</th>

    </tr>
  </thead>
  <tbody>
    @foreach($medias as $media)
    <tr>
    <th scope="row">{{ $media['id'] }}</th>
    <td>{{ $media['created_at'] }}</td>
    <td>{{ $media['description'] }}</td>
    <td>{{ $media['path'] }}</td>
    <td>{{ $media['type'] }}</td>
       <td>
            <button type="button" class="btn btn-danger">  <i class="glyphicon glyphicon-trash" title="Supprimer" style="color:black;"></i> </button>
            <button type="button" class="btn btn-primary">  <i class="glyphicon glyphicon-refresh" title="modifier" style="color:black;"></i> </button></td>
    </tr>
    @endforeach
  </tbody>
</table>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          All Data
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @stop

  @section('script')
  @stop
