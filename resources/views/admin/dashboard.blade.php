
  @extends('admin.baseAdminTemplate')

  @section('title', 'Dashboard')

  @section('css')
  @stop

  @section('content')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashbord Page
        <small>Data Dashboard</small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Users</h3>
          <br>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-succes btn-lg pull-right" href="Ajouter_user">Ajouter</button>

          </div>
        </div>
        <div class="box-body">
      <table class="table table-striped">
  <thead class="thead-dark">
    <tr>
      <th scope="col">id</th>
      <th scope="col">Prénom</th>
      <th scope="col">Nom</th>
      <th scope="col">Email</th>
      <th scope="col">Options</th>
    </tr>
  </thead>
  <tbody>
      @foreach($users as $userDb)
    <tr>
    <th scope="row">{{ $userDb['id'] }}</th>
    <td>{{ $userDb['firstname'] }}</td>
      <td>{{ $userDb['lastname'] }}</td>
      <td>{{ $userDb['email'] }}</td>
       <td>
            <button type="button" class="btn btn-danger">  <i class="glyphicon glyphicon-trash" title="Supprimer" style="color:black;"></i> </button>
            <button type="button" class="btn btn-primary">  <i class="glyphicon glyphicon-refresh" title="modifier" style="color:black;"></i> </button></td>
    </tr>
    @endforeach
  </tbody>
</table>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          All Data
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



  @stop

  @section('script')
  @stop
