@extends('admin.baseAdminTemplate')

  @section('title', 'Add Media')

  @section('css')
  @stop

  @section('content')

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User inscription
        <small>Data Dashboard</small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Ajouter un média</h3>
          <br>
<form action={{ route('adminAddMedia') }} method="POST" enctype="multipart/form-data">
    @csrf

   <div class="form-group">
    <label for="description">Description</label>
    <input type="text" class="form-control" name="description" id="exampleFormControlInput1" placeholder="Name">
  </div>

 <div class="form-group">
    <label for="type"> Le type </label>
    <select name="type" class="form-control">
        <option value='' selected> Choisir un type</option>
        <option value='img'> Image </option>
        <option value='music'> Musique </option>
        <option value='video'> Video </option>
    </select>
  </div>

   <div class="form-group">
    <label for="url">Url</label>
    <input type="text" class="form-control" name="path" id="exampleFormControlInput1" placeholder="Login">
  </div>

  <h4>Ou</h4>

  <div class="form-group">
    <label for="file">Uploader un fichier</label>
    <input type="file" name="file" class="form-control" name="url" id="exampleFormControlInput1" placeholder="Login">
  </div>

 <button type="submit" class="btn btn-primary mb-2">Valider</button>
</form>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  @stop

  @section('script')
  @stop
