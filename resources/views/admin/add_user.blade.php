@extends('admin.baseAdminTemplate')

  @section('title', 'Add User')

  @section('css')
  @stop

  @section('content')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User inscription
        <small>Data Dashboard</small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Ajouter un User</h3>
          <br>
<form action={{ route('adminAddUser') }} method="POST">
    @csrf

   <div class="form-group">
    <label for="firstname">Name</label>
    <input type="text" name="firstname" class="form-control" id="exampleFormControlInput1" placeholder="firstname" required>
    <input type="text" name="lastname" class="form-control" id="exampleFormControlInput1" placeholder="lastname" required>
  </div>

 <div class="form-group">
    <label for="email">Email address</label>
    <input type="email" name="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com" required>
  </div>

   <div class="form-group">
    <label for="password">Password</label>
    <input type="Password" name="password" class="form-control" id="exampleFormControlInput1" placeholder="Password" required>
  </div>

   <div class="form-group">
    <label for="password_confirmation">Password confirmation</label>
    <input type="Password" name="password_confirmation" class="form-control" id="exampleFormControlInput1" placeholder="Password Confirmation " required>
  </div>
 <button type="submit" class="btn btn-primary mb-2">Valider</button>
</form>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @stop

  @section('script')
  @stop
