@extends('admin.baseAdminTemplate')

  @section('title', 'Users')

  @section('css')
  @stop

  @section('content')

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashbord users
        <small>Data Dashboard</small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Title</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
      <table class="table table-striped">
  <thead class="thead-dark">
    <tr>
      <th scope="col">id</th>
      <th scope="col">Nom</th>
      <th scope="col">Prénom</th>
      <th scope="col">Email</th>
       <th scope="col">Login</th>
       <th scope="col">Password</th>
        <th scope="col">Options</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
       <td>@mdo</td>
        <td>@mdo</td>
       <td>  <button type="button" class="btn btn-danger">  <i class="glyphicon glyphicon-trash" title="Supprimer" style="color:black;"></i> </button>
          <button type="button" class="btn btn-primary">  <i class="glyphicon glyphicon-refresh" title="modifier" style="color:black;"></i> </button></td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
       <td>@mdo</td>
        <td>@mdo</td>
         <td>  <button type="button" class="btn btn-danger">  <i class="glyphicon glyphicon-trash" title="Supprimer" style="color:black;"></i> </button>
          <button type="button" class="btn btn-primary">  <i class="glyphicon glyphicon-refresh" title="modifier" style="color:black;"></i> </button></td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
       <td>@mdo</td>
        <td>@mdo</td>
         <td>  <button type="button" class="btn btn-danger">  <i class="glyphicon glyphicon-trash" title="Supprimer" style="color:black;"></i> </button>
          <button type="button" class="btn btn-primary">  <i class="glyphicon glyphicon-refresh" title="modifier" style="color:black;"></i> </button></td>
    </tr>
  </tbody>
</table>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          All Data
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @stop

  @section('script')
  @stop
