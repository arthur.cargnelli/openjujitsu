@extends('baseTemplate')

@section('title', 'Le jujistu')

@section('css')
@stop

@section('content')

<head>
<meta charset='utf-8' />

<link href='{{asset('assets/fullcalendar/packages/core/main.css')}}' rel='stylesheet' />
<link href='{{asset('assets/fullcalendar/packages/daygrid/main.css')}}' rel='stylesheet' />
<link href='{{asset('assets/fullcalendar/packages/timegrid/main.css')}}' rel='stylesheet' />
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">

<script src='{{asset('assets/fullcalendar/packages/core/main.js')}}'></script>
<script src='{{asset('assets/fullcalendar/packages/core/locales/fr.js')}}'></script>

<script src='{{asset('assets/fullcalendar/packages/interaction/main.js')}}'></script>
<script src='{{asset('assets/fullcalendar/packages/daygrid/main.js')}}'></script>
<script src='{{asset('assets/fullcalendar/packages/timegrid/main.js')}}'></script>

<script>
  document.addEventListener('DOMContentLoaded', function() {
    var Calendar = FullCalendar.Calendar;

    // initialize the external events
    var containerEl = document.getElementById('external-events-list');

    // initialize the calendar
    var calendarEl = document.getElementById('calendar');
    var calendar = new Calendar(calendarEl, {
        locale: 'fr',
        plugins: [ 'interaction', 'dayGrid', 'timeGrid'],
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay'
        },
        events: [
			    /*{
            // Monday Tuesday Thursday Friday
            daysOfWeek: ['1', '2', '4', '5'],
				    title: 'Fermé',
            startTime: '20:00:00',
            color: 'red',
          },
          {
            // Wednesday
            daysOfWeek: ['3'],
				    title: 'Fermé',
            startTime: '20:30:00',
            color: 'red',
          },
          {
            // Saturday
            daysOfWeek: ['6'],
				    title: 'Fermé',
            startTime: '19:00:00',
            color: 'red',
          },*/
          {
            // Sunday
            daysOfWeek: ['0'],
				    title: 'Fermé',
            allDay: true,
            color: 'red',
          },
        ]
    });
    calendar.render();

  });

</script>

</head>
<body>
@include('cookieConsent::index')
  <div class="container" id='wrap'>

    <div id='external-events'>

      <div id='external-events-list'>
      </div>

    </div>

    <div id='calendar'></div>

    <div style='clear:both'></div>

  </div>
</body>

@stop

@section('script')
@stop
