@extends('baseTemplate')

@section('title', 'Plan du site')

@section('css')
@stop

@section('content')

<div class="container-text">
    <div class="row">
        <h1>Plan du site</h1>
    </div>
    <div class="row justify-between">
        <div class="col-6">
            <h2>
                <a href="{{ route('home') }}">Accueil</a>
            </h2>
            <h2>
                <a href="{{ route('events') }}">Evènements</a>
            </h2>
            <h2>
                <a href="{{ route('medias') }}">Médias</a>
            </h2>
            <h2>
                <a href="{{ route('jujitsu') }}">Le ju-jitsu</a>
            </h2>
        </div>
        <div class="col-6">
            <h2>
                <a href="{{ route('club') }}">Le club</a>
            </h2>
            <h2>
                <a href="{{ route('contact') }}">Contact</a>
            </h2>
        </div>
    </div>

</div>

@stop

@section('script')
@stop
