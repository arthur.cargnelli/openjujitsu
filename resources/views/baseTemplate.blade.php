<!Doctype html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        @yield('meta')
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="/css/app.css" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">

        @yield('css')
        <title>Open jujitsu</title>
    </head>

    <body>
    @include('cookieConsent::index')

        <!-- Header -->
        <header>
            <nav class="navbar navbar-expand-lg navbar-light bg-header">
                <!-- Logo -->
                <a class="navbar-brand" href="{{ route('home') }}">
                    <img class="logo-header" src="img/logo_text_noire_fond_blanc_persp.png" alt="Logo">
                </a>

                <!-- Toggler/collapsibe Button -->
                <button class="navbar-toggler bg-light" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <!-- Navbar links -->
                <div class="collapse navbar-collapse" id="collapsibleNavbar">
                    <ul class="nav navbar-nav ml-auto menuHeader ">
                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('/') ? 'active' : '' }}" href="{{ route('home')}}">
                                <div class="triangle-left"></div>
                                <div class="carre">Accueil</div>
                                <div class="triangle-right"></div>
                            </a>
                      </li>
                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('events*') ? 'active' : '' }}" href="{{ route('events')}}">
                                <div class="triangle-left"></div>
                                <div class="carre">Evènements</div>
                                <div class="triangle-right"></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('medias*') ? 'active' : '' }}" href="{{ route('medias')}}">
                                <div class="triangle-left"></div>
                                <div class="carre">Médias</div>
                                <div class="triangle-right"></div></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('jujitsu*') ? 'active' : '' }}" href="{{ route('jujitsu') }}">
                                <div class="triangle-left"></div>
                                <div class="carre">Le ju-jitsu</div>
                                <div class="triangle-right"></div></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('club*') ? 'active' : '' }}" href="{{ route('club')}}">
                                <div class="triangle-left"></div>
                                <div class="carre">Le club</div>
                                <div class="triangle-right"></div></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('contact*') ? 'active' : '' }}" href="{{ route('contact') }}">
                                <div class="triangle-left"></div>
                                <div class="carre">Contact</div>
                                <div class="triangle-right"></div>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- header -->

        <div class="content">
            @include('flash_msg')

            @yield('content')
            <!-- Modal HTML embedded directly into document -->
            <div class="modal" id="easter-egg">
                <div>
                    <img class="img-fluid" src="https://http.cat/404"/>
                    <p class="p-easter-egg">Mince ! Je suis encore perdu... </p>
                </div>
            </div>
        </div>

        <!-- Footer -->
        <footer class="page-footer font-small blu4">
            <!-- Footer Links -->
            <div class="container-fluid text-center align-middle ">
            <!-- Grid row -->
            <div class="row">
                <img class="separator-footer" src="img/ligne.png" alt="separator"/>
            </div>
            <div class="row">
                <!-- Grid column -->
                <div class="col-md-2 mt-md-0 mt-3">
                <!-- Content -->
                <img class="logo-footer" src="img/logo_fond_claire.png" alt="logo"/>
            </div>
                <!-- Grid column -->
                <div class="col-md-7 mb-md-0 mb-3 mt-4 font-weight-bold">
                    <a href="{{ route('contact') }}">Contact</a>
                    -
                    <a>Infos Légales</a>
                    -
                    <a href= "{{ route('sitemap') }}">Plan du site</a>
                <!-- Links -->
                </div>
                <!-- Grid column -->
                <div class="col-md-3 mb-md-0 mb-3">
                    <p class="fw-500"> <span class="font-weight-bold fs-20">Open</span> <span class="jujitsu fs-20">Jujitsu</span> <br>
                    <a class="fc-black" href="mailto:contact@openjujitsu.fr">contact<span class="jujitsu">@</span>openjujitsu.fr</a><br>
                        51 Avenue de Madran, 33600 Pessac</p>
                </div>
            </div>
            <!-- Grid row -->
            </div>
            {{-- <!-- Footer Links -->
            <!-- Copyright -->
            <div class="footer-copyright text-center py-3">© 2020 Copyright:
            <a href="https://mdbootstrap.com/"> MDBootstrap.com</a>
            </div>
            <!-- Copyright --> --}}
        </footer>
        <!-- Footer -->

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <!-- jQuery Modal -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
        <script>
            $("header").hover(function(){
                var open = false;
                $(document).mousemove(function(eM){
                    if( eM.pageX > 950 && eM.pageY < 100){
                        open = false;
                        $(window).keydown(function(eK){
                            if(eK.ctrlKey && open == false){
                                $('#easter-egg').modal();
                                $('.close-modal').hide();
                                open = true
                            }
                        })
                    }else{
                        open = true;
                    }
                })
            });
        </script>
            @yield('script')
    </body>
  </html>
