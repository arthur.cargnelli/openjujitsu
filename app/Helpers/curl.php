<?php

if (!function_exists('curl_get')) {
    /**
     * Returns a human readable file size
     *
     * @param string $url
     * url
     *
     * @return array or null
     *
     * */
    function curl_get($url)
    {
        $curl = curl_init();


        $header = array();
        if(isset($_COOKIE['api_token'])){
            $header[] = 'api_token:'.$_COOKIE['api_token'];
        }


        curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => $header
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response,true);
        if($response !== null){
            //return $response['results'][0]['geometry']['location'];
            return $response;
        }else{
            return null;
        }
    }
}

if (!function_exists('curl_post')) {
    /**
     * Returns a human readable file size
     *
     * @param string $url
     * url
     *
     * @param object $opt
     * Data to add in DB
     *
     * @return object or null
     *
     * */
    function curl_post($url,$opt)
    {
        $curl = curl_init();

        $header = array();
        if(isset($_COOKIE['api_token'])){
            $header[] = 'api_token:'.$_COOKIE['api_token'];
        }

        curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_POSTFIELDS => $opt,
        CURLOPT_HTTPHEADER => $header
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        $response = json_decode($response,true);
        if($response !== null){
            //return $response['results'][0]['geometry']['location'];
            return $response;
        }else{
            return null;
        }
    }
}

if (!function_exists('curl_put')) {
    /**
     * Returns a human readable file size
     *
     * @param string $url
     * url
     *
     * @param object $opt
     * Data to update in DB
     *
     * @return object or null
     *
     * */
    function curl_put($url,$opt)
    {
        $curl = curl_init();

        $header = array();
        if(isset($_COOKIE['api_token'])){
            $header[] = 'Authorization: Basic '.$_COOKIE['api_token'];
        }

        curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "PUT",
        CURLOPT_POSTFIELDS => $opt,
        CURLOPT_HTTPHEADER => $header
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        $response = json_decode($response,true);

        if($response !== null){
            //return $response['results'][0]['geometry']['location'];
            return $response;
        }else{
            return null;
        }
    }
}
