<?php

namespace App\Http\Middleware;

use Closure;

class checkAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // call your function
        if(isset($_COOKIE['api_token']) === false){
            return redirect()->route('login')->with('message','Veuillez-vous connectez pour acceder à cette page');
        }
        return $next($request);
    }
}


