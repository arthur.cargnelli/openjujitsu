<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $lastMedias = curl_get(env('API_URL').'/last-medias');
        $lastNews = curl_get(env('API_URL').'/news');
        return view('home', compact('lastMedias','lastNews'));
    }
}
