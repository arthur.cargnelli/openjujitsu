<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NewsController extends Controller
{
    function show(){
        $news = curl_get(env('API_URL').'/news');
        return view('news', compact('news'));
    }
}
