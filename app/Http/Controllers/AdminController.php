<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class AdminController extends Controller
{

    public function login(Request $request){
        $getPassword = curl_get(env("API_URL")."/getHashPassword?email=".$request->input('email'));
        if(Hash::check($request->input('password'), $getPassword['password'])){
            $postRequest = array(
                'email' => $request->input('email'),
                'passwordCheck' => true,
                'secret' => $getPassword['secret']
            );
            $response = curl_post(env("API_URL")."/login",$postRequest);
            if($response['success']){
                $user = array(
                    'firstname' => $response['message']['firstname'],
                    'lastname' => $response['message']['lastname'],
                    'email' => $response['message']['email'],
                    'id' => $response['message']['id'],
                );
                setcookie("api_token", $response['api_token'], time()+(3600*24*15), "/","", 0);
                setcookie("user", json_encode($user), time()+(3600*24*15), "/","", 0);
                return redirect()->route('adminDashboard')
                    ->with('success', 'Vous êtes bien connecté');
            }
        };

    }

    public function register(Request $request){
        $postRequest = array(
            'firstname' => $request->input('firstname'),
            'lastname' => $request->input('lastname'),
            'email' => $request->input('email'),
            'password' => $request->input('password')
        );
        $response = curl_post(env("API_URL")."/register",$postRequest);
        if($response['success']){
            return redirect()->route('dashboard')
                ->with('success', 'Vous êtes bien connecté');
        }
    }

    public function dashboard(){

        $user = json_decode($_COOKIE['user']);
        $users = curl_get(env('API_URL').'/users');
        return view('admin.dashboard', compact('user',('users')));

    }
    public function medias(){

        $user = json_decode($_COOKIE['user']);
        $medias = curl_get(env('API_URL').'/medias');
        return view('admin.medias', compact('user','medias'));

    }

    public function addMedia(Request $request){
        if($request->get('path') != null && $request->file("file") == null){
            $postRequest = array(
                'description' => $request->input('description'),
                'path' => $request->input('path'),
                'type' => $request->input('type')
            );
        }
        elseif ($request->get('path') == null && $request->file("file") != null) {
            $file = $request->file('file');
            $fileName = time().rand(0, 1000).pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $fileName = $fileName.'.'.$file->getClientOriginalExtension();
            $file->move(public_path('uploads/'),$fileName);
            $postRequest = array(
                'description' => $request->input('description'),
                'path' => env("APP_URL")."/uploads".'/'.$fileName,
                'type' => $request->input('type')
            );
        }
        else{
            return back()->with('error','Choose between a Path or a file');
        }
        $response = curl_post(env('API_URL').'/media', $postRequest);
        var_dump($response);
        var_dump($postRequest);
        if($response['success']){
            return redirect()->route('adminMedias')->with('success','Votre média à bien été ajouté');
        }
        ljhlm;
        return back()->with('Error','Un problème est survenu au serveur');
    }
    public function news(){

        $user = json_decode($_COOKIE['user']);
        $news = curl_get(env('API_URL').'/news');
        return view('admin.news', compact('user','news'));
    }

    public function addNews(Request $request){
        $postRequest = array(
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'linkURL' => $request->input('linkURL')
        );
        $news = curl_post(env('API_URL').'/add-news',$postRequest);
        return redirect()->route('adminNews')->with('success','Votre news à bien été ajouté');
    }
    public function users(){

        $user = json_decode($_COOKIE['user']);
        $users = curl_get(env('API_URL').'/users');
        return view('admin.users', compact('user'));

    }
    public function addUser(Request $request){
        if($request->input('password') === $request->input('password_confirmation')){
            $postRequest = array(
                'firstname' => $request->input('firstname'),
                'lastname' => $request->input('lastname'),
                'email' => $request->input('email'),
                'password' => $request->input('password')
            );
            $news = curl_post(env('API_URL').'/add-user',$postRequest);
            return redirect()->route('adminUsers')->with('success','Votre news à bien été ajouté');
        }
        return back()->with('error','Mot de passe et sa confirmation ne sont pas identique');
    }
    public function messages(){

        $user = json_decode($_COOKIE['user']);
        $contacts = curl_get(env('API_URL').'/contacts');
        return view('admin.messages', compact('user','contacts'));

    }
    public function addEvent(Request $request){
        var_dump($request->input('daterange'));
        $dates = explode(' / ',$request->input('daterange'));
        $postRequest = array(
            'event_name' => $request->input('event_name'),
            'description' => $request->input('description'),
            'start_at' => $dates[0],
            'end_at' => $dates[1],
        );
        $response = curl_post(env('API_URL').'/add-event',$postRequest);
        var_dump($response);
        if($response['success']){
            return redirect()->route('adminNews')->with('success','Votre news à bien été ajouté');
        }
    }
    public function newUser(){

        $user = json_decode($_COOKIE['user']);
        return view('admin.add_user', compact('user'));

    }
    public function newMedia(){

        $user = json_decode($_COOKIE['user']);
        return view('admin.add_media', compact('user'));
    }
    public function newEvent(){
        $user = json_decode($_COOKIE['user']);
        return view('admin.add_event', compact('user'));

    }
    public function newNews(){

        $user = json_decode($_COOKIE['user']);
        return view('admin.add_news', compact('user'));
    }

    public function logout(){
        $postRequest = array();
        $response = curl_post(env("API_URL")."/logout",$postRequest);
        if($response['success']){
            unset($_COOKIE['api_token']);
            $res = setcookie('api_token', '', time() - 3600,"/","", 0);
            unset($_COOKIE['user']);
            $res = setcookie('user', '', time() - 3600,"/","", 0);
            return redirect()->route('home')
            ->with('success', 'Vous êtes bien déconnecté');
        }
    }
}
