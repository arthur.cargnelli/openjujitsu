<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FullCalendarController extends Controller
{
    function show(){
        $events = curl_get(env('API_URL').'/events');
        return view('events', compact('events'));
    }
}
