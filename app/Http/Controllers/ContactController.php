<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactController extends Controller
{

    public function send(Request $request){
        $resultsJson = $this->captchaverify($request->input('g-recaptcha-response'));
        if($resultsJson != true){
            return back()->with('message','captcha ERROR');
        }
        else{
            $postRequest = array(
                'firstname' => $request->input('firstname'),
                'lastname' => $request->input('lastname'),
                'email' => $request->input('email'),
                'body' => $request->input('body'),
                'phone' => $request->input('firstname'),
            );
            curl_post(env('API_URL')."/contact",$postRequest);
            return redirect()->route('contact')
                ->with('success', 'Your mail has been send');
        }
    }

    function captchaverify($recaptcha){

        $url = "https://www.google.com/recaptcha/api/siteverify";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array(
            "secret"=>"6LducugUAAAAAE9QlM2aFPhs1onw8ZGjM7Xtf7q7","response"=>$recaptcha));
        $response = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($response);
        return $data->success;
    }
}
